FROM openjdk:8

MAINTAINER xiaokeai

RUN mkdir /app

COPY ./target/ovo-0.0.1-SNAPSHOT.jar /app/app.jar

ENTRYPOINT ["java","-jar","/app/app.jar"]

