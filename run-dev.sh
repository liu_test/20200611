﻿source /etc/profile
#export BUILD_ID=dontKillMe

cd /usr/local/test/20200611
echo "clean package..."
mvn clean package -Dmaven.test.skip=true
echo "开始构建镜像"
docker-compose build local-demo
echo "构建完成..."
docker-compose down local-demo
echo "启动镜像..."
docker-compose up -d local-demo
echo "启动服务成功...."
docker image prune -a -f
exit

