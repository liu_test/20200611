package com.xxbb.ovo.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/route")
public class BrpRouteActualVisitController {


    @GetMapping(value = "/test")
    public String getSalesTypeAndPrecinct( @RequestParam(value = "id", required = true) String id) {

        System.out.println(id);
        return "测试成功了哦~" + id;
    }

}
